import { ScheduledSession, Session, SecondsScheduledRequestBody, PutDefaultSessionRequestBody, DateScheduledRequestBody } from '@common/Requests.spec';

export default class Actions {
    public static getScheduledSessions = async (): Promise<ScheduledSession[]> => {
        let response = await fetch("/scheduledSessions");

        if (response.status !== 200) {
            throw new Error(`Could not get default session: ` + 
                `${response.status} - ${response.statusText}, ${await response.text()}`);
        }

        const scheduledSessions: ScheduledSession[] = await response.json();

        scheduledSessions.forEach(session => {
            session.scheduleDate = new Date(session.scheduleDate);
            session.sessionCreationTime = new Date(session.sessionCreationTime);
        });

        return scheduledSessions;
    }

    public static getDefaultSession = async (): Promise<Session> => {
        let response = await fetch('/defaultSession');

        if (response.status !== 200) {
            throw new Error(`Could not get default session: ` + 
                `${response.status} - ${response.statusText}, ${await response.text()}`);
        }

        return await response.json();
    }
    
    public static setDefaultSession = async (newDefaultSession: Session): Promise<Session> => {
        const body: PutDefaultSessionRequestBody = {
            defaultSession: newDefaultSession
        }

        let response = await fetch('/defaultSession', {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        });

        if (response.status !== 200) {
            throw new Error(`Could not set default session: ` + 
                `${response.status} - ${response.statusText}, ${await response.text()}`);
        }

        return await response.json();
    }

    public static sendSession = async (session: Session): Promise<void> => {
        await Actions.secondsScheduleSession(session, 0);
    }

    public static secondsScheduleSession = async (session: Session, scheduleSeconds: number): Promise<void> => {
        const timerSesionData: SecondsScheduledRequestBody = {
            sessions: [{
                ...session,
                scheduleSeconds
            }]
        };

        let response = await fetch('/schedule/seconds', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(timerSesionData)
        });

        if (response.status !== 200) {
            throw new Error(`Could not seconds-schedule session: ` + 
                `${response.status} - ${response.statusText}, ${await response.text()}`);
        }
    }

    public static dateScheduleSession = async (session: Session, scheduleDate: Date): Promise<void> => {
        const scheduledSesionData: DateScheduledRequestBody = {
            sessions: [{
                ...session,
                scheduleDate: scheduleDate.toJSON()
            }]
        };

        let response = await fetch('/schedule/date', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(scheduledSesionData)
        });

        if (response.status !== 200) {
            throw new Error(`Could not date-schedule session: ` + 
                `${response.status} - ${response.statusText}, ${await response.text()}`);
        }
    }

    public static cancelScheduledSession = async (sessionId: number): Promise<void> => {
        let response = await fetch(`/session/${sessionId}`, {
            method: "DELETE"
        });

        if (response.status !== 200) {
            throw new Error(`Could not cancel scheduled session: ` + 
                `${response.status} - ${response.statusText}, ${await response.text()}`);
        }
    }
}