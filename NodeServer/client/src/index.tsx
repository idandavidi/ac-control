import * as React from "react";
import * as ReactDOM from "react-dom";
import { MonitorContainer } from "Monitor/MonitorContainer";
import { HashRouter as Router, Route, Redirect, Switch, Link, LinkProps, withRouter } from 'react-router-dom';
import { RouteComponentProps } from "react-router";
import { RemoteContainer } from "Remote/RemoteContainer";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import { default as Tab, TabProps } from '@material-ui/core/Tab';
import ListIcon from '@material-ui/icons/ListAlt';
import RemoteControlIcon from '@material-ui/icons/SettingsRemote';
import './reset.css';
import './index.scss';
import '../assets/fonts/icons/flaticon.css';
import { SnackbarProvider } from 'notistack';

const LinkTab: React.ComponentType<TabProps & LinkProps> = Tab as React.ComponentType<TabProps & LinkProps>;

interface AppState {
    currentPath: string;
}

class _AppHeader extends React.Component<RouteComponentProps, AppState> {
    constructor(props: Readonly<RouteComponentProps>) {
        super(props);

        this.state = {
            currentPath: window.location.hash.substr(1) || "/remote",
        };

        this.props.history.listen((location, action) => {
            this.setState({
                currentPath: location.pathname
            });
        });
    }

    render() {
        const { currentPath } = this.state;

        return (
            <AppBar position="fixed" className="app-bar">
                <Tabs value={currentPath} scrollButtons="off">
                    <LinkTab icon={<RemoteControlIcon />} component={Link} to="/remote" value="/remote" />
                    <LinkTab icon={<ListIcon />} component={Link} to="/monitor" value="/monitor" />
                </Tabs>
            </AppBar>
        )
    }
};

const AppHeader = withRouter(_AppHeader);

ReactDOM.render(
    <SnackbarProvider maxSnack={3}>
        <Router>
            <div>
                <AppHeader />
                <Switch>
                    <Route exact path="/remote" component={ RemoteContainer } />
                    <Route exact path="/monitor" component={ MonitorContainer } />
                    <Redirect from="/" to="/remote" />
                </Switch>
            </div>
        </Router>
    </SnackbarProvider>,
    document.getElementById("root")
);