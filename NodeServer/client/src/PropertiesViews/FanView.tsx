import * as React from "react";
import { Fan } from "@common/ACProperties.spec";

export interface FanViewProps {
    fan: Fan;
}

export class FanView extends React.Component<FanViewProps, {}> {
    render() {
        const { fan } = this.props;

        return <img src={`/assets/fonts/icons/vol${fan}.png`} />;
    }
}