import * as React from "react";
import { Power } from "@common/ACProperties.spec";

export interface PowerViewProps {
    power: Power;
}

export class PowerView extends React.Component<PowerViewProps, {}> {
    render() {
        const { power } = this.props;

        if (power === Power.ON) {
            return (<i className="flaticon-power on" style={{color: "rgb(112, 173, 71)"}}></i>);
        } 

        return (<i className="flaticon-power off" style={{color: "rgb(255, 51, 51)"}}></i>);
    }
}