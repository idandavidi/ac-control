import * as React from "react";
import { Mode } from "@common/ACProperties.spec";

export interface ModeViewProps {
    mode: Mode;
}

export class ModeView extends React.Component<ModeViewProps, {}> {
    render() {
        const { mode } = this.props;

        switch(mode) {
            case Mode.AUTO:
                return <span>A</span>;
            case Mode.COLD:
                return <i className="flaticon-snow"></i>;
            case Mode.DRY:
                return <i className="flaticon-rain"></i>;
            case Mode.FAN:
                return <i className="flaticon-fan"></i>;
            case Mode.HEAT:
                return <i className="flaticon-sun"></i>;
        }
    }
}