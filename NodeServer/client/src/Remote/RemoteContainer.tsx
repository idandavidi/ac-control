import * as React from "react";
import { Remote } from "./Remote";
import { Session } from '@common/Requests.spec';
import Actions from "server/Actions";

export interface RemoteContainerState {
    isLoading: boolean;
    defaultSession: Session;
}

export class RemoteContainer extends React.Component<{}, RemoteContainerState> {
    constructor(props: Readonly<{}>) {
        super(props);

        this.state = {
            isLoading: true,
            defaultSession: null
        }

        this.loadData();
    }

    loadData = async (): Promise<void> => {
        const defaultSession = await Actions.getDefaultSession();

        this.setState({
            isLoading: false,
            defaultSession
        });
    }

    setDefaultSession = async (newDefaultSession: Session) => {
        newDefaultSession = await Actions.setDefaultSession(newDefaultSession);
        this.setState({
            defaultSession: newDefaultSession
        })
    };

    render() {
        const { isLoading, defaultSession } = this.state;

        if (isLoading) {
            return <div>Loading..</div>
        }

        return <Remote defaultSession={ defaultSession } 
                sendSession={ Actions.sendSession } 
                secondsScheduleSession = { Actions.secondsScheduleSession }
                dateScheduleSession={ Actions.dateScheduleSession }
                setDefault={ this.setDefaultSession } />;
    }
}