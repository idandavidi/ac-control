import * as React from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import * as moment from 'moment';
import './TimerDialog.scss'

export interface TimerDialogProps {
    onClose: () => void;
    onSend: (scheduleData: Date) => void;
}

export interface TimerDialogState {
    hours: number;
}

export class TimerDialog extends React.Component<TimerDialogProps, TimerDialogState> {
    constructor(props: Readonly<TimerDialogProps>) {
        super(props);

        this.state = {
            hours: 1.5
        }
    }

    render() {
        const { onClose, onSend } = this.props;
        const { hours } = this.state;

        return (<div className="timer-dialog">
            <DialogContent className="dialog-content">
                <form noValidate>
                    <div className="form-row">
                        <TextField label="Hours" type="number" value={ hours } required
                                onChange={(e) => { 
                                    this.setState({ hours: parseFloat(e.target.value) })
                                }} />

                        <div className="hours-buttons-row">
                            <Button variant="contained" size="small" onClick={ () => { this.setState({ hours: 1 })} }>
                                1
                            </Button>
                            <Button variant="contained" size="small" onClick={ () => { this.setState({ hours: 1.5 })} }>
                                1.5
                            </Button>
                            <Button variant="contained" size="small" onClick={ () => { this.setState({ hours: 2 })} }>
                                2
                            </Button>
                        </div>
                    </div>
                </form>                
            </DialogContent>
                <footer className="dialog-footer">
                    { this.isDateValid() ?
                        <span>The session will be scheduled to {this.getFullDate().toLocaleString()}.</span> :
                        <span className="date-error">The selected date is invalid, please choose a future date.</span>
                    }
                <DialogActions>
                    <Button variant="contained" onClick={onClose}>
                        Cancel
                    </Button>
                    <Button variant="contained" color="primary" onClick={() => {onSend(this.getFullDate())}} disabled={ !this.isDateValid() }>
                        Scheudle
                    </Button>
                </DialogActions>
            </footer>
        </div>);
    }

    private getFullDate = () : Date => {
        return new Date(new Date().getTime() + this.state.hours * 3600 * 1000);
    }

    private isDateValid = () : boolean => {
        return this.state.hours > 0;
    }
}