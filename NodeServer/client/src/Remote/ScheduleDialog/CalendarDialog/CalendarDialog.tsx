import * as React from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import * as moment from 'moment';
import './CalendarDialog.scss'

export interface CalendarDialogProps {
    onClose: () => void;
    onSend: (scheduleData: Date) => void;
}

export interface CalendarDialogState {
    selectedDate: string;
    selectedTime: string;
}

export class CalendarDialog extends React.Component<CalendarDialogProps, CalendarDialogState> {
    constructor(props: Readonly<CalendarDialogProps>) {
        super(props);

        this.state = {
            selectedDate: this.getDefaultDate(),
            selectedTime: this.getDefaultHour()
        }
    }

    render() {
        const { onClose, onSend } = this.props;
        const { selectedDate, selectedTime } = this.state;

        return (<div className="calendar-dialog">
            <DialogContent className="dialog-content">
                <form noValidate>
                    <div className="form-row">
                        <TextField label="Date" type="date" value={ selectedDate } required
                                onChange={(e) => { 
                                    this.setState({ selectedDate: e.target.value })
                                }} />

                        <div className="date-buttons-row">
                            <Button variant="contained" size="small" onClick={ this.setToday }>
                                Today
                            </Button>
                            <Button variant="contained" size="small" onClick={ this.setTomorrow } >
                                Tommorow
                            </Button>
                        </div>
                    </div>

                    <div className="form-row">
                        <TextField label="Time" type="time" value={ selectedTime } 
                                onChange={(e) => { 
                                    this.setState({ selectedTime: e.target.value })
                                }}/>
                    </div>
                </form>

            </DialogContent>
            <footer className="dialog-footer">
                { this.isDateValid() ?
                    <div>The session will be scheduled to {this.getFullDate().toLocaleString()}.</div> :
                    <div className="date-error">The selected date is invalid, please choose a future date.</div>
                }
                <DialogActions>
                    <Button variant="contained" onClick={onClose}>
                        Cancel
                    </Button>
                    <Button variant="contained" color="primary" onClick={() => {onSend(this.getFullDate())}} disabled={ !this.isDateValid() }>
                        Scheudle
                    </Button>
                </DialogActions>
            </footer>
        </div>);
    }

    private setTomorrow = (): void => {
        this.setState({
            selectedDate: moment(new Date(new Date().getTime() + 24 * 60 * 60 * 1000)).format('YYYY-MM-DD')
        });
    }

    private setToday = (): void => {
        this.setState({
            selectedDate: moment(new Date()).format('YYYY-MM-DD')
        });
    }

    private getDefaultFullDate = (): Date => {
        // The recommended default date is half and an hour later
        return new Date((new Date()).getTime() + 5400 * 1000);
    }

    private getDefaultDate = (): string => {
        return moment(this.getDefaultFullDate()).format('YYYY-MM-DD');
    }

    private getDefaultHour = (): string => {
        return moment(this.getDefaultFullDate()).format('HH:mm')
    }

    private getFullDate = () : Date => {
        const { selectedDate , selectedTime } = this.state;
        let fullDate: string = `${selectedDate}T${selectedTime}:00`
        return new Date(fullDate);
    }

    private isDateValid = () : boolean => {
        return (this.getFullDate() > new Date());
    }
}