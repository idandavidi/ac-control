import * as React from "react";
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CalendarIcon from '@material-ui/icons/CalendarToday';
import TimerIcon from '@material-ui/icons/Timer';
import { CalendarDialog } from "./CalendarDialog/CalendarDialog";
import { TimerDialog } from "./TimerDialog/TimerDialog";
import './ScheduleDialog.scss';

export interface ScheduleDialogProps {
    open: boolean;
    onClose: () => void;
    onSend: (scheduleData: Date) => void;
}

export interface ScheduleDialogState {
    scheduleType: ScheduleType;
    calendarDialog: JSX.Element;
    timerDialog: JSX.Element;
}

enum ScheduleType {
    CALENDAR,
    TIMER
}

export class ScheduleDialog extends React.Component<ScheduleDialogProps, ScheduleDialogState> {

    calendarDialog = <CalendarDialog onClose={()=>{}} onSend={()=>{}} />;
    timerDialog = <CalendarDialog onClose={()=>{}} onSend={()=>{}} />;

    constructor(props: Readonly<ScheduleDialogProps>) {
        super(props);

        const { onClose, onSend } = props;

        this.state = {
            scheduleType: ScheduleType.CALENDAR,
            calendarDialog: React.createElement(CalendarDialog, { onClose, onSend }),
            timerDialog: React.createElement(TimerDialog, { onClose, onSend }),
        }
    }

    render() {
        const { open, onClose } = this.props;
        const { scheduleType, calendarDialog, timerDialog } = this.state;

        return (<Dialog className="schedule-dialog" classes={{paper: "paper"}} open={open} onClose={onClose} >
            <DialogTitle className="form-dialog-title">Schedule a Session</DialogTitle>

            <Tabs
                value={this.state.scheduleType}
                onChange={(event, value) => { this.setState({ scheduleType: value })}}
                fullWidth
                indicatorColor="primary"
                textColor="primary"
            >
                <Tab icon={<CalendarIcon />} label="Calendar" value={ScheduleType.CALENDAR} />
                <Tab icon={<TimerIcon />} label="Timer" value={ScheduleType.TIMER} />
            </Tabs>

            {(() => {
                switch(scheduleType) {
                    case ScheduleType.CALENDAR:
                        return calendarDialog;
                    case ScheduleType.TIMER:
                        return timerDialog;
                }
            })()}

        </Dialog>);
    }
}