import * as React from "react";
import { Session } from "@common/Requests.spec";
import IconButton from '@material-ui/core/IconButton';
import { Power, Fan, Mode } from "@common/ACProperties.spec";
import { FanView } from "PropertiesViews/FanView";
import { PowerView } from "PropertiesViews/PowerView";
import { ModeView } from "PropertiesViews/ModeView";
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import { ScheduleDialog } from "./ScheduleDialog/ScheduleDialog";
import { InjectedNotistackProps, withSnackbar } from 'notistack';
import { QuickSessions } from "./QuickSessions/QuickSessions";
import './Remote.scss';

export interface RemoteProps {
    defaultSession: Session;
    sendSession: (session: Session) => Promise<void>;
    secondsScheduleSession: (session: Session, scheduleSeconds: number) => Promise<void>;
    dateScheduleSession: (session: Session, scheduleDate: Date) => Promise<void>;
    setDefault: (newDefaultSession: Session) => Promise<void>;
}

export interface RemoteState {
    currentSession: Session;
    isInstantSend: boolean;
    isSetDefaultLoading: boolean;
    isScheduleDialogOpen: boolean;
}

class _Remote extends React.Component<RemoteProps & InjectedNotistackProps, RemoteState> {
    constructor(props: Readonly<RemoteProps & InjectedNotistackProps>) {
        super(props);

        this.state = {
            currentSession: { ...props.defaultSession },
            isInstantSend: false,
            isSetDefaultLoading: false,
            isScheduleDialogOpen: false
        }
    }

    render() {
        const { isInstantSend, isSetDefaultLoading, isScheduleDialogOpen } = this.state;
        const { temperature, power, fan, mode } = this.state.currentSession;

        return <div className="remote-container">
            <div className="remote">
                <ScheduleDialog open={isScheduleDialogOpen} 
                        onClose={() => { this.closeScheduleDialog()}} 
                        onSend={(scheduleDate: Date) => { this.scheduleSession(scheduleDate) }} />
                <div className="temperature-container">
                    <IconButton className="operator"
                            onClick={() => { this.setSessionProperties({ temperature: Math.min(temperature + 1, 30) }) }}
                            disabled={ temperature >= 30}>
                        +
                    </IconButton>
                    <h1 className="value">{temperature}&deg;</h1>
                    <IconButton className="operator"
                            onClick={() => { this.setSessionProperties({ temperature: Math.max(temperature -1, 16) }) }}
                            disabled={ temperature <= 16 }>
                        -
                    </IconButton>
                </div>
                <div className="properties-row">
                    <IconButton className="operator"
                            onClick={() => { this.setSessionProperties({ power: this.getNextEnumValue(Power, power) }) }}>
                        <PowerView power={ power } />
                    </IconButton>
                    <IconButton className="operator"
                            onClick={() => { this.setSessionProperties({ fan: this.getNextEnumValue(Fan, fan) }) }}>
                        <FanView fan={ fan } />
                    </IconButton>
                    <IconButton className="operator"
                            onClick={() => { this.setSessionProperties({ mode: this.getNextEnumValue(Mode, mode) }) }}>
                        <ModeView mode={ mode } />
                    </IconButton>
                </div>
                <div className="actions">
                    <div className="actions-row">
                        <Button variant="contained" color="primary" 
                                disabled={ isInstantSend } 
                                onClick={ this.sendSession }>
                            Send
                        </Button>
                        <Button variant="contained" color="primary"
                                onClick={() => { this.setState({ isScheduleDialogOpen: true })}}>
                            Schedule
                        </Button>
                        <Button variant="contained" color="secondary" onClick={ this.powerOff }>
                            Power OFF
                        </Button>
                    </div>
                    <div className="actions-row">
                        <Tooltip title="Set as default">
                            <Button variant="contained" onClick={ () => { this.setDefault() }} disabled={ isSetDefaultLoading }>
                                Default
                            </Button>
                        </Tooltip>
                        <ToggleButtonGroup className="instant-send-toggle" 
                                exclusive onChange={() => this.setState({isInstantSend: !isInstantSend})} 
                                value={ isInstantSend }>
                            <ToggleButton value={ true }>
                                Instant Send
                            </ToggleButton>
                        </ToggleButtonGroup>
                        <Tooltip title="Reset to default">
                            <Button variant="contained" onClick={() => { this.reset() }}>
                                Reset
                            </Button>
                        </Tooltip>
                    </div>
                </div>
            </div>
            <QuickSessions secondsScheduleSession={this.props.secondsScheduleSession} session={this.state.currentSession} />
        </div>
    }

    /**
     * 
     * @param theEnum Returns the next enum value, by order.
     * WORKS ONLY IF THIS IS A NUMERIC SEQUENCED ENUM THAT STARTS WITH 0 AND INCREASED BY 1 EACH TIME!
     */
    private getNextEnumValue<TEnum>(theEnum: TEnum, currentValue: number) {
        const enumLength = Object.keys(theEnum).length / 2
        return (currentValue + 1) % enumLength;
    }

    private setSessionProperties = async (properties: Partial<Session>) => {
        const newSession: Session = {
            ...this.state.currentSession,
            ...properties
        };

        this.setState( { currentSession: newSession }, () => {
            if (this.state.isInstantSend) {
                this.sendSession();
            }
        });
    }

    private sendSession = async () => {
        try {
            await this.props.sendSession({ ...this.state.currentSession });
            this.props.onPresentSnackbar('success', 'Sent to device');
        } catch (e) {
            this.props.onPresentSnackbar('error', 'Failed to send the command to device');
        }
    }

    private scheduleSession = async (scheduleDate: Date) => {
        try {
            await this.props.dateScheduleSession({ ...this.state.currentSession }, scheduleDate);
            this.closeScheduleDialog();
            this.props.onPresentSnackbar('success', 'Command scheduled');
        } catch (e) {
            this.props.onPresentSnackbar('error', 'Failed to schedule the command on the server');
        }
    }

    private closeScheduleDialog = () => {
        this.setState({ isScheduleDialogOpen: false });
    }

    private powerOff = async () => {
        try {
            await this.props.sendSession({ 
                ...this.props.defaultSession,
                power: Power.OFF
            });

            this.props.onPresentSnackbar('success', 'Sent to device');
        } catch (e) {
            this.props.onPresentSnackbar('error', 'Failed to send the command to device');
            console.error(e);
        }
    }

    private reset = () => {
        this.setState ({
            currentSession: { ...this.props.defaultSession }
        });
    }

    private setDefault = async () => {
        this.setState({
            isSetDefaultLoading: true
        });

        try {
            await this.props.setDefault(this.state.currentSession);
            this.props.onPresentSnackbar('success', 'Set as default');
        } catch(e) {
            this.props.onPresentSnackbar('error', `Failed to set as default`);
        } finally {
            this.setState({
                isSetDefaultLoading: false
            });
        }
    }
}

export const Remote = withSnackbar(_Remote);