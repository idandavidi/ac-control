import * as React from "react";
import { Session } from "@common/Requests.spec";
import Button from '@material-ui/core/Button';
import NavigationIcon from '@material-ui/icons/Navigation';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blueGrey from '@material-ui/core/colors/blueGrey';
import { InjectedNotistackProps, withSnackbar } from 'notistack';
import { Power } from "@common/ACProperties.spec";
import './QuickSessions.scss'

const theme = createMuiTheme({
    palette: {
        primary: blueGrey,
    },
    overrides: {
        MuiButton: {
            raised: {
                color: 'white',
            },
        },
    }
});

export interface QuickSessionsProps {
    session: Session;
    secondsScheduleSession: (session: Session, scheduleSeconds: number) => Promise<void>;
}

export interface QuickSessionsState {
    isLoading: boolean;
}

export class _QuickSessions extends React.Component<QuickSessionsProps & InjectedNotistackProps, QuickSessionsState> {
    constructor(props: Readonly<QuickSessionsProps & InjectedNotistackProps>) {
        super(props);

        this.state = {
            isLoading: false
        }
    }

    render() {
        const { isLoading } = this.state;

        return <MuiThemeProvider theme={theme}>
            <div className="quick-session-buttons-container">
                <Button variant="raised" color="primary" disabled={isLoading} onClick={() => { this.schedule(1.5) }}>
                    <NavigationIcon />
                    Schedule 1.5 Hours
                </Button>
                <Button variant="raised" color="primary" disabled={isLoading} onClick={() => { this.schedule(2) }}>
                    <NavigationIcon />
                    Schedule 2 Hours
                </Button>
                <Button variant="raised" color="primary" disabled={isLoading} onClick={() => { this.schedule(3) }}>
                    <NavigationIcon />
                    Schedule 3 Hours
                </Button>
            </div>
        </MuiThemeProvider>
    }

    private schedule = async (hours: number) => {
        this.setState({
            isLoading: true
        });

        try {
            const powerOnSession: Session = {
                ...this.props.session,
                power: Power.ON
            }
            await this.props.secondsScheduleSession(powerOnSession, 0);

            const powerOffSession: Session = {
                ...this.props.session,
                power: Power.OFF
            }
            let scheduledSeconds = hours * 3600;
            await this.props.secondsScheduleSession(powerOffSession, scheduledSeconds);
            this.props.onPresentSnackbar('success', 'Command scheduled');
        } catch (e) {
            this.props.onPresentSnackbar('error', 'Failed to schedule the command on the server');
        } finally {
            this.setState({
                isLoading: false
            });
        }
    }
}

export const QuickSessions = withSnackbar(_QuickSessions);