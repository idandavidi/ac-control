import * as React from "react";

import { Monitor } from "./Monitor";
import { ScheduledSession } from '@common/Requests.spec';
import Actions from "server/Actions";

export interface MonitorContainerState {
    isLoading: boolean;
    scheduledSessions: ScheduledSession[];
}

export class MonitorContainer extends React.Component<{}, MonitorContainerState> {

    constructor(props: Readonly<{}>) {
        super(props);

        this.state = {
            isLoading: true,
            scheduledSessions: []
        }

        this.loadData();
    }

    loadData = async (): Promise<void> => {
        const scheduledSessions = await Actions.getScheduledSessions();

        this.setState({
            isLoading: false,
            scheduledSessions
        });
    }

    render() {
        const { isLoading, scheduledSessions } = this.state;

        if (isLoading) {
            return <div>Loading..</div>
        }

        return <Monitor scheduledSessions={scheduledSessions}
                onCancelScheduledSession={this.onCancelScheduledSession} />;
    }

    onCancelScheduledSession = async (sessionId: number): Promise<void> => {
        await Actions.cancelScheduledSession(sessionId);
        this.loadData();
    }
}