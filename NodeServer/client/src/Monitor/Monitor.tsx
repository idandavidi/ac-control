import * as React from "react";
import { ScheduledSession } from "@common/Requests.spec";
import { ScheduledSessionView } from "./ScheduledSessionView/ScheduledSessionView";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { InjectedNotistackProps, withSnackbar } from 'notistack';
import './Monitor.scss';

export interface MonitorProps { 
    scheduledSessions: ScheduledSession[];
    onCancelScheduledSession: (sessionId: number) => Promise<void>;
}

export interface MonitorState {
    cancelDialogOpen: boolean;
    cancelSessionId?: number;
}

class _Monitor extends React.Component<MonitorProps & InjectedNotistackProps, MonitorState> {
    constructor(props: Readonly<MonitorProps & InjectedNotistackProps>) {
        super(props);

        this.state = {
            cancelDialogOpen: false
        }
    }

    render() {
        const { scheduledSessions } = this.props;

        return <div className="future-scheduled-sessions">
            <div className="title">
                <h2>Future Scheduled Sessions</h2>
            </div>
            <div className="sessions-container">
                {scheduledSessions.length ?
                    scheduledSessions
                    .sort((a, b) => (a.scheduleDate.getTime() - b.scheduleDate.getTime()))
                    .map(session => 
                        <ScheduledSessionView 
                            key={session.id} 
                            onCancelScheduledSession={() => this.openCancelDialog(session.id)}
                            {...session} />
                    ) :
                    <span>0 scheduled sessions found.</span>
                }
            </div>
            <Dialog open={this.state.cancelDialogOpen} onClose={this.closeCancelDialog} >
                <DialogTitle>Cancel Session</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {`Are you sure you want to cancel and remove session #${this.state.cancelSessionId}?`}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(this.onConfirmClicked)} color="primary">
                        Yes
                    </Button>
                    <Button onClick={this.closeCancelDialog} color="primary" autoFocus>
                        No
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    }

    openCancelDialog = (sessionId: number): void => {
        this.setState({
            cancelDialogOpen: true,
            cancelSessionId: sessionId
        });
    }

    closeCancelDialog = (): void => {
        this.setState({
            cancelDialogOpen: false,
        });
    }

    onConfirmClicked = async (): Promise<void> => {
        try {
            await this.props.onCancelScheduledSession(this.state.cancelSessionId)
            this.closeCancelDialog();
            this.props.onPresentSnackbar('success', `Session #${this.state.cancelSessionId} canceled`);
        } catch (e) {
            this.props.onPresentSnackbar('error', 'Failed to cancel the session');
        }
    }
}

export const Monitor = withSnackbar(_Monitor);