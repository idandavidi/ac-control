import * as React from "react";
import { ScheduledSession } from '@common/Requests.spec';
import { FanView } from "PropertiesViews/FanView";
import { PowerView } from "PropertiesViews/PowerView";
import { ModeView } from "PropertiesViews/ModeView";
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Avatar from '@material-ui/core/Avatar';
import * as moment from 'moment';
import './ScheduledSessionView.scss';

export interface ScheduledSessionProps extends ScheduledSession { 
    onCancelScheduledSession: () => void;
}

export interface ScheduledSessionState { }

export class ScheduledSessionView extends React.Component<ScheduledSessionProps, ScheduledSessionState> {

    constructor(props: Readonly<ScheduledSessionProps>) {
        super(props);
    }

    render() {
        let { id, power, temperature, fan, mode, sessionCreationTime, scheduleDate, onCancelScheduledSession } = this.props;

        return <div className="scheduled-session-container">
                <div className="id-container">
                    <div className="session-property-view">
                        <div className="value"><Avatar className="avatar">{id}</Avatar></div>
                    </div>
                </div>
            <div className="properties-container">
                <div className="properties-container-row">
                    <div className="session-property-view">
                        <div className="value"><PowerView power={power} /></div>
                        <div className="id">Power</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value">{ temperature }&deg;</div>
                        <div className="id">Temperature</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value"><FanView fan={ fan } /></div>
                        <div className="id">Fan</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value"><ModeView mode={ mode } /></div>
                        <div className="id">Mode</div>
                    </div>
                </div>
                <div className="properties-container-row">
                    <div className="session-property-view">
                        <div className="value date">
                            <div>{ moment(scheduleDate).format('DD/MM/YYYY') }</div>
                            <div>{ moment(scheduleDate).format('LTS') }</div>
                        </div>
                        <div className="id date">Schedule Date</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value date">
                            <div>{ moment(sessionCreationTime).format('DD/MM/YYYY') }</div>
                            <div>{ moment(sessionCreationTime).format('LTS') }</div>
                        </div>
                        <div className="id date">Creation Date</div>
                    </div>
                </div>
            </div>
            <div className="buttons-container">
                <IconButton onClick={onCancelScheduledSession}>
                    <DeleteIcon />
                </IconButton>
            </div>
        </div>;
    }
}