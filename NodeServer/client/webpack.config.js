const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
    entry: './src/index.tsx',
    output: {
        filename: 'bundle.js',
        path: __dirname + "/dist"
    },
    mode: "development",
    devtool: "source-map",
    resolve: {
        extensions: [".tsx", ".ts", ".js", ".json"],
        plugins: [new TsconfigPathsPlugin({ /*configFile: "./path/to/tsconfig.json" */ })]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
        }),
        new CopyWebpackPlugin([
            { from: 'assets', to: 'assets' }
        ])
    ],
    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader','css-loader']
        }, {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            exclude: /node_modules/,
        }, {
            test: /\.scss$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        }]
    },
    devServer: {
        port: 3030,
        host: '0.0.0.0',
        proxy: [{
            context: ['/**'],
            target: 'http://localhost:3000',
            secure: false, //don't use https
        }],
    }
};