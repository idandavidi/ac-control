import { Power, Mode, Fan } from "./ACProperties.spec";

export interface Session {
    power: Power;
    temperature: number;
    mode: Mode;
    fan: Fan;
}

export interface SecondsScheduledSessionRequest extends Partial<Session> {
    scheduleSeconds: number; // Time left in seconds to trigger the session
}

export interface DateScheduledSessionRequest extends Partial<Session> {
    scheduleDate: string; // Date representing string, for example `2018-09-29T19:29:00.956`
}

export interface ScheduledSession extends Session {
    id: number;
    scheduleDate: Date;
    sessionCreationTime: Date;
}

export interface SecondsScheduledRequestBody {
    sessions: SecondsScheduledSessionRequest[];
}

export interface DateScheduledRequestBody {
    sessions: DateScheduledSessionRequest[];
}

export interface PutDefaultSessionRequestBody {
    defaultSession: Session;
}