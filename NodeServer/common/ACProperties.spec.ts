export enum Power {
    OFF = 0,
    ON = 1
}

export enum Mode {
    AUTO = 0,
    COLD = 1,
    DRY = 2,
    FAN = 3,
    HEAT = 4
}

export enum Fan {
    FAN_AUTO = 0,
    FAN_1 = 1,
    FAN_2 = 2,
    FAN_3 = 3
}