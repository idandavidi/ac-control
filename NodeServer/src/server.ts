import { config as dotenvConfig } from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import { Request, Response } from 'express';
import Storage from './storage';
import { Arduino } from './ArduinoSDK';
import { SecondsScheduledRequestBody, ScheduledSession, DateScheduledRequestBody, PutDefaultSessionRequestBody, Session } from '@common/Requests.spec';

dotenvConfig(); // enable reading environment variables from .env file

const app: express.Application = express();

const initApp = async () => {
    const port: number = parseInt(process.env.APP_PORT || "3000");
    const storage: Storage = await (new Storage()).init();
    const arduino: Arduino = new Arduino(process.env.ARDUINO_HOST as string, 
        await storage.getScheduledSessions(), storage);

    app.use(bodyParser.json());
    app.listen(port, () => {
        console.log(`Listening at http://localhost:${port}/`);
    });
    app.use(express.static('client/dist'));

    app.post('/schedule/seconds', async (req: Request, res: Response) => {
        const body: SecondsScheduledRequestBody = req.body;

        if (!body.sessions || body.sessions.length == 0) {
            res.status(400);
            res.send(`body.sessions array is not specified or empty.`);
        }

        let secondsScheduledSessions = body.sessions;

        // Convert seconds-scheduled session to standard scheduled session
        const now = new Date();
        const scheduledSessions: ScheduledSession[] = []; 
        const defaultSession = await storage.getDefaultSession();

        // Convert seconds to date and add session creation time and id
        // Using traditional for..of because .map with async/await is not waiting
        // to each of inner function to be done before execting the next function,
        // causing getting the same id for all of the sessions.
        for (let secondsScheduledSession of secondsScheduledSessions) {
            if (!secondsScheduledSession.hasOwnProperty('scheduleSeconds')) {
                res.status(400);
                res.send(`scheduleSeconds is missing in session.`);
                return;
            }

            const { scheduleSeconds, ...scheduledSession} = {
                ...defaultSession,
                ...secondsScheduledSession,
                id: await storage.getNextId(),
                sessionCreationTime: now,
                scheduleDate: new Date(now.getTime() + secondsScheduledSession.scheduleSeconds * 1000)
            };
            scheduledSessions.push(scheduledSession);
        }

        // Schedule each session
        for (let scheduledSession of scheduledSessions) {
            try {
                await arduino.scheduleCommand(scheduledSession);
            } catch (error) {
                res.status(500);
                res.send(`Could not schedule this session, check the server logs.`);
                return;
            }
        };

        const numberOfScheduledSessions = (await storage.getScheduledSessions()).length;
        res.send(`OK - ${numberOfScheduledSessions}`);
    });

    app.post('/schedule/date', async (req: Request, res: Response) => {
        const body: DateScheduledRequestBody = req.body;

        if (!body.sessions || body.sessions.length == 0) {
            res.status(400);
            res.send(`body.sessions array is not specified or empty.`);
        }

        const dateScheduledSessions = body.sessions;
        const scheduledSessions: ScheduledSession[] = [];
        const defaultSession = await storage.getDefaultSession();

        // Add session creation time and id
        // Using traditional for..of because .map with async/await is not waiting
        // to each of inner function to be done before execting the next function,
        // causing getting the same id for all of the sessions.
        for (let dateScheduledSession of dateScheduledSessions) {
            if (!dateScheduledSession.scheduleDate) {
                res.status(400);
                res.send(`scheduleDate is missing in session.`);
                return;
            }

            scheduledSessions.push({
                ...defaultSession,
                ...dateScheduledSession,
                id: await storage.getNextId(),
                sessionCreationTime: new Date(),
                scheduleDate: new Date(dateScheduledSession.scheduleDate)
            });
        }

        // Schedule each session
        for (let scheduledSession of scheduledSessions) {
            try {
                await arduino.scheduleCommand(scheduledSession);
            } catch (error) {
                res.status(500);
                res.send(`Could not schedule this session, check the server logs.`);
                return;
            }
        };

        const numberOfScheduledSessions = (await storage.getScheduledSessions()).length;
        res.send(`OK - ${numberOfScheduledSessions}`);
    });

    app.get('/numberOfScheduledSessions', async (req: Request, res: Response) => {
        const numberOfScheduledSessions = (await storage.getScheduledSessions()).length;
        res.send(String(numberOfScheduledSessions));
    });

    app.get('/scheduledSessions', async (req: Request, res: Response) => {
        const scheduledSessions: ScheduledSession[] = await storage.getScheduledSessions();

        res.status(200);
        res.contentType('application/json');
        res.send(scheduledSessions);
    });

    app.delete('/allScheduledSessions', async (req: Request, res: Response) => {
        const scheduledSessions: ScheduledSession[] = await storage.getScheduledSessions();

        for (let i = 0; i < scheduledSessions.length; ++i) {
          await arduino.cancelScheduledSession(scheduledSessions[i].id);
        }
        
        res.send("OK");
    });

    app.get('/defaultSession', async (req: Request, res: Response) => {
        const defaultSession = await storage.getDefaultSession();
        
        res.status(200);
        res.contentType('application/json');
        res.send(defaultSession);
    });

    app.put('/defaultSession', async (req: Request, res: Response) => {
        const body: PutDefaultSessionRequestBody = req.body;
        const currentDefaultSession = await storage.getDefaultSession();
        const newDefaultSession: Session = {
            ...currentDefaultSession,
            ...body.defaultSession
        }

        await storage.setDefaultSession(newDefaultSession);

        res.status(200);
        res.contentType('application/json');
        res.send(newDefaultSession);
    });

    app.delete('/session/:id', async (req: Request, res: Response) => {
        if (isNaN(req.params.id)) {
            res.status(400);
            res.send(`id ${req.params.id} is not a number.`);
            return;
        }

        const sessionId = parseInt(req.params.id);
        await arduino.cancelScheduledSession(sessionId);

        res.status(200);
        res.send('OK');
    });
};

initApp();