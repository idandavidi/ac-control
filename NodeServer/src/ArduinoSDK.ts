import fetch from 'node-fetch';
import Storage from "./storage";
import { ScheduledSession, Session } from '@common/Requests.spec';

export class Arduino {
    private arduinoUrl: string;
    private timers: Map<number, NodeJS.Timer>; // map of session id and its timer
    private storage: Storage;

    constructor(arduinoHost: string, scheduledSessions: ScheduledSession[],
            storage: Storage) {
        this.arduinoUrl = `http://${arduinoHost}`;
        this.storage = storage;
        this.timers = new Map<number, NodeJS.Timer>();

        this.initScheduledSessions(scheduledSessions);
    }

    private async initScheduledSessions(scheduledSessions: ScheduledSession[]): Promise<void> {
        for (let session of scheduledSessions) {
            await this.scheduleCommand(session);
        };
    }

    public async scheduleCommand(scheduledSession: ScheduledSession): Promise<void> {
        const now = new Date();

        const { scheduleDate, sessionCreationTime, ...session } = scheduledSession;

        const scheduleInSeconds = (scheduleDate.getTime() - now.getTime()) / 1000;

        // Enabling upto 3 seconds delay for immediate sessions 
        if (scheduleInSeconds >= -3 && scheduleInSeconds <= 0) {
            try {
                await this.sendCommand(session);
                console.info(`Session ${scheduledSession.id} has been successfully sent to Arduino.`);
            } catch (error) {
                console.error(`Could not send command to arduino, session: ${JSON.stringify(scheduledSession)}`, error);
                throw error;
            }
        } else if (scheduleInSeconds > 0) {
            const timer: NodeJS.Timer = setTimeout(async () => {
                try {
                    await this.sendCommand(session);
                    console.info(`Session ${scheduledSession.id} has been successfully sent to Arduino.`);
                } catch (error) {
                    console.error(`Could not send command to arduino, session: ${JSON.stringify(scheduledSession)}`, error);
                } finally {
                    await this.removeSession(scheduledSession.id);
                }
            }, scheduleInSeconds * 1000);
            await this.saveSession(scheduledSession, timer);
            console.info(`Session ${scheduledSession.id} has been scheduled to ${scheduledSession.scheduleDate}.`);
        } else {
            await this.removeSession(scheduledSession.id);
            console.warn(`Warning - scheduleInSeconds value is less than zero: ${scheduleInSeconds}, \n` + 
                `\tsession: ${JSON.stringify(scheduledSession)} \n` + 
                `\tThis session has been removed.`);
        }
    }

    public async cancelScheduledSession(sessionId: number): Promise<void> {
        await this.removeSession(sessionId);

        console.log(`Session ${sessionId} has been canceled.`);
    }

    private async sendCommand(session: Session): Promise<void> {
        const uri = `${this.arduinoUrl}/command?` + 
            `temperature=${session.temperature}&mode=${session.mode}&fanSpeed=${session.fan}&power=${session.power}`;

        const response = await fetch(uri, { method: 'POST' });
        const data = await response.body;

        if (!response.ok) {
            const text = await response.text();
            throw new Error(`Error from arduino: ${text}`)
        }
    }

    private async saveSession(scheduledSession: ScheduledSession, timer: NodeJS.Timer) {
        await this.storage.addScheduledSession(scheduledSession);
        this.timers.set(scheduledSession.id, timer);
    }

    private async removeSession(sessionId: number) {
        // TODO add reason and save to logs
        await this.storage.removeScheduledSession(sessionId);
        clearTimeout(this.timers.get(sessionId)!);
        this.timers.delete(sessionId);
    }
}