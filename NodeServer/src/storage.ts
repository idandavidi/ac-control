import StoragePersistance from 'node-persist';
import { Session, ScheduledSession } from '@common/Requests.spec';

interface StorageData {
    defaultSession: Session;
    scheduledSessions: ScheduledSession[];
    sessionsCounter: number;
}

export default class Storage {
    private readonly STORAGE_KEY = 'storage';
    private readonly STORAGE_DIR = 'storage';
    private readonly defaultStorage: StorageData = {
        defaultSession: {
            power: 0,
            temperature: 25,
            mode: 1,
            fan: 1
        },
        scheduledSessions: [],
        sessionsCounter: 0
    }

    public init = async (): Promise<Storage> => {
        await StoragePersistance.init({
            dir: this.STORAGE_DIR
        });

        if (await StoragePersistance.length() == 0) {
            await StoragePersistance.setItem(this.STORAGE_KEY, this.defaultStorage);
        }

        return this;
    }

    private getStorageData = async (): Promise<StorageData> => {
        let storage = await StoragePersistance.getItem(this.STORAGE_KEY) as StorageData;

        // Coverting string-date to date objects
        storage.scheduledSessions.forEach(session => {
            session.scheduleDate = new Date(session.scheduleDate);
            session.sessionCreationTime = new Date(session.sessionCreationTime);
        });

        return storage;
    }

    public getNextId = async (): Promise<number> => {
        let sessionsCounter = (await this.getStorageData()).sessionsCounter;
        sessionsCounter++;

        await this.updateStorage({ sessionsCounter });
        return sessionsCounter;
    }

    public getDefaultSession = async (): Promise<Session> => {
        return (await this.getStorageData()).defaultSession;
    }

    public setDefaultSession = async (defaultSession: Session): Promise<void> => {
        await this.updateStorage({
            defaultSession
        });

        console.log(`Default session has been set: ${JSON.stringify(defaultSession)}.`);
    }

    public getScheduledSessions = async () : Promise<ScheduledSession[]> => {
        return (await this.getStorageData()).scheduledSessions;
    }

    public addScheduledSession = async (session: ScheduledSession) : Promise<void> => {
        let scheduledSessions =  (await this.getStorageData()).scheduledSessions;

        // When the system boots up, the scheduled session are re-scheduled, so this
        // check avoids adding the sessions again to the storage.
        const isAlreadySaved: boolean = scheduledSessions.find(s => s.id === session.id) ? true : false;
        if (!isAlreadySaved) {
            scheduledSessions.push(session);
            await this.updateStorage({ scheduledSessions });
        }
    }
    
    /**
     * Removes a scheduled session from the storage.
     * If the session doesn't exist - the function does nothing.
     */
    public removeScheduledSession = async (sessionId: number) : Promise<void> => {
        let scheduledSessions =  (await this.getStorageData()).scheduledSessions;

        scheduledSessions = scheduledSessions.filter(s => s.id !== sessionId);
        await this.updateStorage({ scheduledSessions });
    }

    private async updateStorage(updatedData: Partial<StorageData>) {
        const storage = await this.getStorageData();

        await StoragePersistance.setItem(this.STORAGE_KEY, {
            ...storage,
            ...updatedData
        });
    }
}