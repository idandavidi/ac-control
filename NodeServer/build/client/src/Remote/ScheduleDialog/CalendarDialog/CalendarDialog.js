"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var DialogActions_1 = __importDefault(require("@material-ui/core/DialogActions"));
var DialogContent_1 = __importDefault(require("@material-ui/core/DialogContent"));
var moment = __importStar(require("moment"));
require("./CalendarDialog.scss");
var CalendarDialog = /** @class */ (function (_super) {
    __extends(CalendarDialog, _super);
    function CalendarDialog(props) {
        var _this = _super.call(this, props) || this;
        _this.setTomorrow = function () {
            _this.setState({
                selectedDate: moment(new Date(new Date().getTime() + 24 * 60 * 60 * 1000)).format('YYYY-MM-DD')
            });
        };
        _this.setToday = function () {
            _this.setState({
                selectedDate: moment(new Date()).format('YYYY-MM-DD')
            });
        };
        _this.getDefaultFullDate = function () {
            // The recommended default date is half and an hour later
            return new Date((new Date()).getTime() + 5400 * 1000);
        };
        _this.getDefaultDate = function () {
            return moment(_this.getDefaultFullDate()).format('YYYY-MM-DD');
        };
        _this.getDefaultHour = function () {
            return moment(_this.getDefaultFullDate()).format('HH:mm');
        };
        _this.getFullDate = function () {
            var _a = _this.state, selectedDate = _a.selectedDate, selectedTime = _a.selectedTime;
            var fullDate = selectedDate + "T" + selectedTime + ":00";
            return new Date(fullDate);
        };
        _this.isDateValid = function () {
            return (_this.getFullDate() > new Date());
        };
        _this.state = {
            selectedDate: _this.getDefaultDate(),
            selectedTime: _this.getDefaultHour()
        };
        return _this;
    }
    CalendarDialog.prototype.render = function () {
        var _this = this;
        var _a = this.props, onClose = _a.onClose, onSend = _a.onSend;
        var _b = this.state, selectedDate = _b.selectedDate, selectedTime = _b.selectedTime;
        return (<div className="calendar-dialog">
            <DialogContent_1.default className="dialog-content">
                <form noValidate>
                    <div className="form-row">
                        <TextField_1.default label="Date" type="date" value={selectedDate} required onChange={function (e) {
            _this.setState({ selectedDate: e.target.value });
        }}/>

                        <div className="date-buttons-row">
                            <Button_1.default variant="contained" size="small" onClick={this.setToday}>
                                Today
                            </Button_1.default>
                            <Button_1.default variant="contained" size="small" onClick={this.setTomorrow}>
                                Tommorow
                            </Button_1.default>
                        </div>
                    </div>

                    <div className="form-row">
                        <TextField_1.default label="Time" type="time" value={selectedTime} onChange={function (e) {
            _this.setState({ selectedTime: e.target.value });
        }}/>
                    </div>
                </form>

            </DialogContent_1.default>
            <footer className="dialog-footer">
                {this.isDateValid() ?
            <div>The session will be scheduled to {this.getFullDate().toLocaleString()}.</div> :
            <div className="date-error">The selected date is invalid, please choose a future date.</div>}
                <DialogActions_1.default>
                    <Button_1.default variant="contained" onClick={onClose}>
                        Cancel
                    </Button_1.default>
                    <Button_1.default variant="contained" color="primary" onClick={function () { onSend(_this.getFullDate()); }} disabled={!this.isDateValid()}>
                        Scheudle
                    </Button_1.default>
                </DialogActions_1.default>
            </footer>
        </div>);
    };
    return CalendarDialog;
}(React.Component));
exports.CalendarDialog = CalendarDialog;
