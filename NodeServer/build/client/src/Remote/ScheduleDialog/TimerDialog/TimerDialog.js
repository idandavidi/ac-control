"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var DialogActions_1 = __importDefault(require("@material-ui/core/DialogActions"));
var DialogContent_1 = __importDefault(require("@material-ui/core/DialogContent"));
require("./TimerDialog.scss");
var TimerDialog = /** @class */ (function (_super) {
    __extends(TimerDialog, _super);
    function TimerDialog(props) {
        var _this = _super.call(this, props) || this;
        _this.getFullDate = function () {
            return new Date(new Date().getTime() + _this.state.hours * 3600 * 1000);
        };
        _this.isDateValid = function () {
            return _this.state.hours > 0;
        };
        _this.state = {
            hours: 1.5
        };
        return _this;
    }
    TimerDialog.prototype.render = function () {
        var _this = this;
        var _a = this.props, onClose = _a.onClose, onSend = _a.onSend;
        var hours = this.state.hours;
        return (<div className="timer-dialog">
            <DialogContent_1.default className="dialog-content">
                <form noValidate>
                    <div className="form-row">
                        <TextField_1.default label="Hours" type="number" value={hours} required onChange={function (e) {
            _this.setState({ hours: parseFloat(e.target.value) });
        }}/>

                        <div className="hours-buttons-row">
                            <Button_1.default variant="contained" size="small" onClick={function () { _this.setState({ hours: 1 }); }}>
                                1
                            </Button_1.default>
                            <Button_1.default variant="contained" size="small" onClick={function () { _this.setState({ hours: 1.5 }); }}>
                                1.5
                            </Button_1.default>
                            <Button_1.default variant="contained" size="small" onClick={function () { _this.setState({ hours: 2 }); }}>
                                2
                            </Button_1.default>
                        </div>
                    </div>
                </form>                
            </DialogContent_1.default>
                <footer className="dialog-footer">
                    {this.isDateValid() ?
            <span>The session will be scheduled to {this.getFullDate().toLocaleString()}.</span> :
            <span className="date-error">The selected date is invalid, please choose a future date.</span>}
                <DialogActions_1.default>
                    <Button_1.default variant="contained" onClick={onClose}>
                        Cancel
                    </Button_1.default>
                    <Button_1.default variant="contained" color="primary" onClick={function () { onSend(_this.getFullDate()); }} disabled={!this.isDateValid()}>
                        Scheudle
                    </Button_1.default>
                </DialogActions_1.default>
            </footer>
        </div>);
    };
    return TimerDialog;
}(React.Component));
exports.TimerDialog = TimerDialog;
