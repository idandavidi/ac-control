"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Dialog_1 = __importDefault(require("@material-ui/core/Dialog"));
var DialogTitle_1 = __importDefault(require("@material-ui/core/DialogTitle"));
var Tabs_1 = __importDefault(require("@material-ui/core/Tabs"));
var Tab_1 = __importDefault(require("@material-ui/core/Tab"));
var CalendarToday_1 = __importDefault(require("@material-ui/icons/CalendarToday"));
var Timer_1 = __importDefault(require("@material-ui/icons/Timer"));
var CalendarDialog_1 = require("./CalendarDialog/CalendarDialog");
var TimerDialog_1 = require("./TimerDialog/TimerDialog");
require("./ScheduleDialog.scss");
var ScheduleType;
(function (ScheduleType) {
    ScheduleType[ScheduleType["CALENDAR"] = 0] = "CALENDAR";
    ScheduleType[ScheduleType["TIMER"] = 1] = "TIMER";
})(ScheduleType || (ScheduleType = {}));
var ScheduleDialog = /** @class */ (function (_super) {
    __extends(ScheduleDialog, _super);
    function ScheduleDialog(props) {
        var _this = _super.call(this, props) || this;
        _this.calendarDialog = <CalendarDialog_1.CalendarDialog onClose={function () { }} onSend={function () { }}/>;
        _this.timerDialog = <CalendarDialog_1.CalendarDialog onClose={function () { }} onSend={function () { }}/>;
        var onClose = props.onClose, onSend = props.onSend;
        _this.state = {
            scheduleType: ScheduleType.CALENDAR,
            calendarDialog: React.createElement(CalendarDialog_1.CalendarDialog, { onClose: onClose, onSend: onSend }),
            timerDialog: React.createElement(TimerDialog_1.TimerDialog, { onClose: onClose, onSend: onSend }),
        };
        return _this;
    }
    ScheduleDialog.prototype.render = function () {
        var _this = this;
        var _a = this.props, open = _a.open, onClose = _a.onClose;
        var _b = this.state, scheduleType = _b.scheduleType, calendarDialog = _b.calendarDialog, timerDialog = _b.timerDialog;
        return (<Dialog_1.default className="schedule-dialog" classes={{ paper: "paper" }} open={open} onClose={onClose}>
            <DialogTitle_1.default className="form-dialog-title">Schedule a Session</DialogTitle_1.default>

            <Tabs_1.default value={this.state.scheduleType} onChange={function (event, value) { _this.setState({ scheduleType: value }); }} fullWidth indicatorColor="primary" textColor="primary">
                <Tab_1.default icon={<CalendarToday_1.default />} label="Calendar" value={ScheduleType.CALENDAR}/>
                <Tab_1.default icon={<Timer_1.default />} label="Timer" value={ScheduleType.TIMER}/>
            </Tabs_1.default>

            {(function () {
            switch (scheduleType) {
                case ScheduleType.CALENDAR:
                    return calendarDialog;
                case ScheduleType.TIMER:
                    return timerDialog;
            }
        })()}

        </Dialog_1.default>);
    };
    return ScheduleDialog;
}(React.Component));
exports.ScheduleDialog = ScheduleDialog;
