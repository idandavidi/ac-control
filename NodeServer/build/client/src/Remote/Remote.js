"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var IconButton_1 = __importDefault(require("@material-ui/core/IconButton"));
var ACProperties_spec_1 = require("@common/ACProperties.spec");
var FanView_1 = require("PropertiesViews/FanView");
var PowerView_1 = require("PropertiesViews/PowerView");
var ModeView_1 = require("PropertiesViews/ModeView");
var Tooltip_1 = __importDefault(require("@material-ui/core/Tooltip"));
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var ToggleButton_1 = __importDefault(require("@material-ui/lab/ToggleButton"));
var ToggleButtonGroup_1 = __importDefault(require("@material-ui/lab/ToggleButtonGroup"));
var ScheduleDialog_1 = require("./ScheduleDialog/ScheduleDialog");
var notistack_1 = require("notistack");
var QuickSessions_1 = require("./QuickSessions/QuickSessions");
require("./Remote.scss");
var _Remote = /** @class */ (function (_super) {
    __extends(_Remote, _super);
    function _Remote(props) {
        var _this = _super.call(this, props) || this;
        _this.setSessionProperties = function (properties) { return __awaiter(_this, void 0, void 0, function () {
            var newSession;
            var _this = this;
            return __generator(this, function (_a) {
                newSession = __assign({}, this.state.currentSession, properties);
                this.setState({ currentSession: newSession }, function () {
                    if (_this.state.isInstantSend) {
                        _this.sendSession();
                    }
                });
                return [2 /*return*/];
            });
        }); };
        _this.sendSession = function () { return __awaiter(_this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.props.sendSession(__assign({}, this.state.currentSession))];
                    case 1:
                        _a.sent();
                        this.props.onPresentSnackbar('success', 'Sent to device');
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.props.onPresentSnackbar('error', 'Failed to send the command to device');
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.scheduleSession = function (scheduleDate) { return __awaiter(_this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.props.dateScheduleSession(__assign({}, this.state.currentSession), scheduleDate)];
                    case 1:
                        _a.sent();
                        this.closeScheduleDialog();
                        this.props.onPresentSnackbar('success', 'Command scheduled');
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this.props.onPresentSnackbar('error', 'Failed to schedule the command on the server');
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.closeScheduleDialog = function () {
            _this.setState({ isScheduleDialogOpen: false });
        };
        _this.powerOff = function () { return __awaiter(_this, void 0, void 0, function () {
            var e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.props.sendSession(__assign({}, this.props.defaultSession, { power: ACProperties_spec_1.Power.OFF }))];
                    case 1:
                        _a.sent();
                        this.props.onPresentSnackbar('success', 'Sent to device');
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        this.props.onPresentSnackbar('error', 'Failed to send the command to device');
                        console.error(e_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.reset = function () {
            _this.setState({
                currentSession: __assign({}, _this.props.defaultSession)
            });
        };
        _this.setDefault = function () { return __awaiter(_this, void 0, void 0, function () {
            var e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setState({
                            isSetDefaultLoading: true
                        });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        return [4 /*yield*/, this.props.setDefault(this.state.currentSession)];
                    case 2:
                        _a.sent();
                        this.props.onPresentSnackbar('success', 'Set as default');
                        return [3 /*break*/, 5];
                    case 3:
                        e_4 = _a.sent();
                        this.props.onPresentSnackbar('error', "Failed to set as default");
                        return [3 /*break*/, 5];
                    case 4:
                        this.setState({
                            isSetDefaultLoading: false
                        });
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        }); };
        _this.state = {
            currentSession: __assign({}, props.defaultSession),
            isInstantSend: false,
            isSetDefaultLoading: false,
            isScheduleDialogOpen: false
        };
        return _this;
    }
    _Remote.prototype.render = function () {
        var _this = this;
        var _a = this.state, isInstantSend = _a.isInstantSend, isSetDefaultLoading = _a.isSetDefaultLoading, isScheduleDialogOpen = _a.isScheduleDialogOpen;
        var _b = this.state.currentSession, temperature = _b.temperature, power = _b.power, fan = _b.fan, mode = _b.mode;
        return <div className="remote-container">
            <div className="remote">
                <ScheduleDialog_1.ScheduleDialog open={isScheduleDialogOpen} onClose={function () { _this.closeScheduleDialog(); }} onSend={function (scheduleDate) { _this.scheduleSession(scheduleDate); }}/>
                <div className="temperature-container">
                    <IconButton_1.default className="operator" onClick={function () { _this.setSessionProperties({ temperature: Math.min(temperature + 1, 30) }); }} disabled={temperature >= 30}>
                        +
                    </IconButton_1.default>
                    <h1 className="value">{temperature}&deg;</h1>
                    <IconButton_1.default className="operator" onClick={function () { _this.setSessionProperties({ temperature: Math.max(temperature - 1, 16) }); }} disabled={temperature <= 16}>
                        -
                    </IconButton_1.default>
                </div>
                <div className="properties-row">
                    <IconButton_1.default className="operator" onClick={function () { _this.setSessionProperties({ power: _this.getNextEnumValue(ACProperties_spec_1.Power, power) }); }}>
                        <PowerView_1.PowerView power={power}/>
                    </IconButton_1.default>
                    <IconButton_1.default className="operator" onClick={function () { _this.setSessionProperties({ fan: _this.getNextEnumValue(ACProperties_spec_1.Fan, fan) }); }}>
                        <FanView_1.FanView fan={fan}/>
                    </IconButton_1.default>
                    <IconButton_1.default className="operator" onClick={function () { _this.setSessionProperties({ mode: _this.getNextEnumValue(ACProperties_spec_1.Mode, mode) }); }}>
                        <ModeView_1.ModeView mode={mode}/>
                    </IconButton_1.default>
                </div>
                <div className="actions">
                    <div className="actions-row">
                        <Button_1.default variant="contained" color="primary" disabled={isInstantSend} onClick={this.sendSession}>
                            Send
                        </Button_1.default>
                        <Button_1.default variant="contained" color="primary" onClick={function () { _this.setState({ isScheduleDialogOpen: true }); }}>
                            Schedule
                        </Button_1.default>
                        <Button_1.default variant="contained" color="secondary" onClick={this.powerOff}>
                            Power OFF
                        </Button_1.default>
                    </div>
                    <div className="actions-row">
                        <Tooltip_1.default title="Set as default">
                            <Button_1.default variant="contained" onClick={function () { _this.setDefault(); }} disabled={isSetDefaultLoading}>
                                Default
                            </Button_1.default>
                        </Tooltip_1.default>
                        <ToggleButtonGroup_1.default className="instant-send-toggle" exclusive onChange={function () { return _this.setState({ isInstantSend: !isInstantSend }); }} value={isInstantSend}>
                            <ToggleButton_1.default value={true}>
                                Instant Send
                            </ToggleButton_1.default>
                        </ToggleButtonGroup_1.default>
                        <Tooltip_1.default title="Reset to default">
                            <Button_1.default variant="contained" onClick={function () { _this.reset(); }}>
                                Reset
                            </Button_1.default>
                        </Tooltip_1.default>
                    </div>
                </div>
            </div>
            <QuickSessions_1.QuickSessions secondsScheduleSession={this.props.secondsScheduleSession} session={this.state.currentSession}/>
        </div>;
    };
    /**
     *
     * @param theEnum Returns the next enum value, by order.
     * WORKS ONLY IF THIS IS A NUMERIC SEQUENCED ENUM THAT STARTS WITH 0 AND INCREASED BY 1 EACH TIME!
     */
    _Remote.prototype.getNextEnumValue = function (theEnum, currentValue) {
        var enumLength = Object.keys(theEnum).length / 2;
        return (currentValue + 1) % enumLength;
    };
    return _Remote;
}(React.Component));
exports.Remote = notistack_1.withSnackbar(_Remote);
