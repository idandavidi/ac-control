"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var ACProperties_spec_1 = require("@common/ACProperties.spec");
var ModeView = /** @class */ (function (_super) {
    __extends(ModeView, _super);
    function ModeView() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ModeView.prototype.render = function () {
        var mode = this.props.mode;
        switch (mode) {
            case ACProperties_spec_1.Mode.AUTO:
                return <span>A</span>;
            case ACProperties_spec_1.Mode.COLD:
                return <i className="flaticon-snow"></i>;
            case ACProperties_spec_1.Mode.DRY:
                return <i className="flaticon-rain"></i>;
            case ACProperties_spec_1.Mode.FAN:
                return <i className="flaticon-fan"></i>;
            case ACProperties_spec_1.Mode.HEAT:
                return <i className="flaticon-sun"></i>;
        }
    };
    return ModeView;
}(React.Component));
exports.ModeView = ModeView;
