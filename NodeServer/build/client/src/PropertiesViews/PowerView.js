"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var ACProperties_spec_1 = require("@common/ACProperties.spec");
var PowerView = /** @class */ (function (_super) {
    __extends(PowerView, _super);
    function PowerView() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PowerView.prototype.render = function () {
        var power = this.props.power;
        if (power === ACProperties_spec_1.Power.ON) {
            return (<i className="flaticon-power on" style={{ color: "rgb(112, 173, 71)" }}></i>);
        }
        return (<i className="flaticon-power off" style={{ color: "rgb(255, 51, 51)" }}></i>);
    };
    return PowerView;
}(React.Component));
exports.PowerView = PowerView;
