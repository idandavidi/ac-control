"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var FanView_1 = require("PropertiesViews/FanView");
var PowerView_1 = require("PropertiesViews/PowerView");
var ModeView_1 = require("PropertiesViews/ModeView");
var IconButton_1 = __importDefault(require("@material-ui/core/IconButton"));
var Delete_1 = __importDefault(require("@material-ui/icons/Delete"));
var Avatar_1 = __importDefault(require("@material-ui/core/Avatar"));
var moment = __importStar(require("moment"));
require("./ScheduledSessionView.scss");
var ScheduledSessionView = /** @class */ (function (_super) {
    __extends(ScheduledSessionView, _super);
    function ScheduledSessionView(props) {
        return _super.call(this, props) || this;
    }
    ScheduledSessionView.prototype.render = function () {
        var _a = this.props, id = _a.id, power = _a.power, temperature = _a.temperature, fan = _a.fan, mode = _a.mode, sessionCreationTime = _a.sessionCreationTime, scheduleDate = _a.scheduleDate, onCancelScheduledSession = _a.onCancelScheduledSession;
        return <div className="scheduled-session-container">
                <div className="id-container">
                    <div className="session-property-view">
                        <div className="value"><Avatar_1.default className="avatar">{id}</Avatar_1.default></div>
                    </div>
                </div>
            <div className="properties-container">
                <div className="properties-container-row">
                    <div className="session-property-view">
                        <div className="value"><PowerView_1.PowerView power={power}/></div>
                        <div className="id">Power</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value">{temperature}&deg;</div>
                        <div className="id">Temperature</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value"><FanView_1.FanView fan={fan}/></div>
                        <div className="id">Fan</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value"><ModeView_1.ModeView mode={mode}/></div>
                        <div className="id">Mode</div>
                    </div>
                </div>
                <div className="properties-container-row">
                    <div className="session-property-view">
                        <div className="value date">
                            <div>{moment(scheduleDate).format('DD/MM/YYYY')}</div>
                            <div>{moment(scheduleDate).format('LTS')}</div>
                        </div>
                        <div className="id date">Schedule Date</div>
                    </div>
                    <div className="session-property-view">
                        <div className="value date">
                            <div>{moment(sessionCreationTime).format('DD/MM/YYYY')}</div>
                            <div>{moment(sessionCreationTime).format('LTS')}</div>
                        </div>
                        <div className="id date">Creation Date</div>
                    </div>
                </div>
            </div>
            <div className="buttons-container">
                <IconButton_1.default onClick={onCancelScheduledSession}>
                    <Delete_1.default />
                </IconButton_1.default>
            </div>
        </div>;
    };
    return ScheduledSessionView;
}(React.Component));
exports.ScheduledSessionView = ScheduledSessionView;
