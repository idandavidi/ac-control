"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var ReactDOM = __importStar(require("react-dom"));
var MonitorContainer_1 = require("Monitor/MonitorContainer");
var react_router_dom_1 = require("react-router-dom");
var RemoteContainer_1 = require("Remote/RemoteContainer");
var AppBar_1 = __importDefault(require("@material-ui/core/AppBar"));
var Tabs_1 = __importDefault(require("@material-ui/core/Tabs"));
var Tab_1 = __importDefault(require("@material-ui/core/Tab"));
var ListAlt_1 = __importDefault(require("@material-ui/icons/ListAlt"));
var SettingsRemote_1 = __importDefault(require("@material-ui/icons/SettingsRemote"));
require("./reset.css");
require("./index.scss");
require("../assets/fonts/icons/flaticon.css");
var notistack_1 = require("notistack");
var LinkTab = Tab_1.default;
var _AppHeader = /** @class */ (function (_super) {
    __extends(_AppHeader, _super);
    function _AppHeader(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            currentPath: window.location.hash.substr(1) || "/remote",
        };
        _this.props.history.listen(function (location, action) {
            _this.setState({
                currentPath: location.pathname
            });
        });
        return _this;
    }
    _AppHeader.prototype.render = function () {
        var currentPath = this.state.currentPath;
        return (<AppBar_1.default position="fixed" className="app-bar">
                <Tabs_1.default value={currentPath} scrollButtons="off">
                    <LinkTab icon={<SettingsRemote_1.default />} component={react_router_dom_1.Link} to="/remote" value="/remote"/>
                    <LinkTab icon={<ListAlt_1.default />} component={react_router_dom_1.Link} to="/monitor" value="/monitor"/>
                </Tabs_1.default>
            </AppBar_1.default>);
    };
    return _AppHeader;
}(React.Component));
;
var AppHeader = react_router_dom_1.withRouter(_AppHeader);
ReactDOM.render(<notistack_1.SnackbarProvider maxSnack={3}>
        <react_router_dom_1.HashRouter>
            <div>
                <AppHeader />
                <react_router_dom_1.Switch>
                    <react_router_dom_1.Route exact path="/remote" component={RemoteContainer_1.RemoteContainer}/>
                    <react_router_dom_1.Route exact path="/monitor" component={MonitorContainer_1.MonitorContainer}/>
                    <react_router_dom_1.Redirect from="/" to="/remote"/>
                </react_router_dom_1.Switch>
            </div>
        </react_router_dom_1.HashRouter>
    </notistack_1.SnackbarProvider>, document.getElementById("root"));
