"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var Actions = /** @class */ (function () {
    function Actions() {
    }
    Actions.getScheduledSessions = function () { return __awaiter(_this, void 0, void 0, function () {
        var response, _a, _b, _c, scheduledSessions;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, fetch("/scheduledSessions")];
                case 1:
                    response = _d.sent();
                    if (!(response.status !== 200)) return [3 /*break*/, 3];
                    _a = Error.bind;
                    _b = "Could not get default session: ";
                    _c = response.status + " - " + response.statusText + ", ";
                    return [4 /*yield*/, response.text()];
                case 2: throw new (_a.apply(Error, [void 0, _b +
                        (_c + (_d.sent()))]))();
                case 3: return [4 /*yield*/, response.json()];
                case 4:
                    scheduledSessions = _d.sent();
                    scheduledSessions.forEach(function (session) {
                        session.scheduleDate = new Date(session.scheduleDate);
                        session.sessionCreationTime = new Date(session.sessionCreationTime);
                    });
                    return [2 /*return*/, scheduledSessions];
            }
        });
    }); };
    Actions.getDefaultSession = function () { return __awaiter(_this, void 0, void 0, function () {
        var response, _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, fetch('/defaultSession')];
                case 1:
                    response = _d.sent();
                    if (!(response.status !== 200)) return [3 /*break*/, 3];
                    _a = Error.bind;
                    _b = "Could not get default session: ";
                    _c = response.status + " - " + response.statusText + ", ";
                    return [4 /*yield*/, response.text()];
                case 2: throw new (_a.apply(Error, [void 0, _b +
                        (_c + (_d.sent()))]))();
                case 3: return [4 /*yield*/, response.json()];
                case 4: return [2 /*return*/, _d.sent()];
            }
        });
    }); };
    Actions.setDefaultSession = function (newDefaultSession) { return __awaiter(_this, void 0, void 0, function () {
        var body, response, _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    body = {
                        defaultSession: newDefaultSession
                    };
                    return [4 /*yield*/, fetch('/defaultSession', {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify(body)
                        })];
                case 1:
                    response = _d.sent();
                    if (!(response.status !== 200)) return [3 /*break*/, 3];
                    _a = Error.bind;
                    _b = "Could not set default session: ";
                    _c = response.status + " - " + response.statusText + ", ";
                    return [4 /*yield*/, response.text()];
                case 2: throw new (_a.apply(Error, [void 0, _b +
                        (_c + (_d.sent()))]))();
                case 3: return [4 /*yield*/, response.json()];
                case 4: return [2 /*return*/, _d.sent()];
            }
        });
    }); };
    Actions.sendSession = function (session) { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Actions.secondsScheduleSession(session, 0)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); };
    Actions.secondsScheduleSession = function (session, scheduleSeconds) { return __awaiter(_this, void 0, void 0, function () {
        var timerSesionData, response, _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    timerSesionData = {
                        sessions: [__assign({}, session, { scheduleSeconds: scheduleSeconds })]
                    };
                    return [4 /*yield*/, fetch('/schedule/seconds', {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify(timerSesionData)
                        })];
                case 1:
                    response = _d.sent();
                    if (!(response.status !== 200)) return [3 /*break*/, 3];
                    _a = Error.bind;
                    _b = "Could not seconds-schedule session: ";
                    _c = response.status + " - " + response.statusText + ", ";
                    return [4 /*yield*/, response.text()];
                case 2: throw new (_a.apply(Error, [void 0, _b +
                        (_c + (_d.sent()))]))();
                case 3: return [2 /*return*/];
            }
        });
    }); };
    Actions.dateScheduleSession = function (session, scheduleDate) { return __awaiter(_this, void 0, void 0, function () {
        var scheduledSesionData, response, _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    scheduledSesionData = {
                        sessions: [__assign({}, session, { scheduleDate: scheduleDate.toJSON() })]
                    };
                    return [4 /*yield*/, fetch('/schedule/date', {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify(scheduledSesionData)
                        })];
                case 1:
                    response = _d.sent();
                    if (!(response.status !== 200)) return [3 /*break*/, 3];
                    _a = Error.bind;
                    _b = "Could not date-schedule session: ";
                    _c = response.status + " - " + response.statusText + ", ";
                    return [4 /*yield*/, response.text()];
                case 2: throw new (_a.apply(Error, [void 0, _b +
                        (_c + (_d.sent()))]))();
                case 3: return [2 /*return*/];
            }
        });
    }); };
    Actions.cancelScheduledSession = function (sessionId) { return __awaiter(_this, void 0, void 0, function () {
        var response, _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, fetch("/session/" + sessionId, {
                        method: "DELETE"
                    })];
                case 1:
                    response = _d.sent();
                    if (!(response.status !== 200)) return [3 /*break*/, 3];
                    _a = Error.bind;
                    _b = "Could not cancel scheduled session: ";
                    _c = response.status + " - " + response.statusText + ", ";
                    return [4 /*yield*/, response.text()];
                case 2: throw new (_a.apply(Error, [void 0, _b +
                        (_c + (_d.sent()))]))();
                case 3: return [2 /*return*/];
            }
        });
    }); };
    return Actions;
}());
exports.default = Actions;
