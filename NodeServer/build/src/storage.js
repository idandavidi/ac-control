"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_persist_1 = __importDefault(require("node-persist"));
var Storage = /** @class */ (function () {
    function Storage() {
        var _this = this;
        this.STORAGE_KEY = 'storage';
        this.STORAGE_DIR = 'storage';
        this.defaultStorage = {
            defaultSession: {
                power: 0,
                temperature: 25,
                mode: 1,
                fan: 1
            },
            scheduledSessions: [],
            sessionsCounter: 0
        };
        this.init = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, node_persist_1.default.init({
                            dir: this.STORAGE_DIR
                        })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, node_persist_1.default.length()];
                    case 2:
                        if (!((_a.sent()) == 0)) return [3 /*break*/, 4];
                        return [4 /*yield*/, node_persist_1.default.setItem(this.STORAGE_KEY, this.defaultStorage)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, this];
                }
            });
        }); };
        this.getStorageData = function () { return __awaiter(_this, void 0, void 0, function () {
            var storage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, node_persist_1.default.getItem(this.STORAGE_KEY)];
                    case 1:
                        storage = _a.sent();
                        // Coverting string-date to date objects
                        storage.scheduledSessions.forEach(function (session) {
                            session.scheduleDate = new Date(session.scheduleDate);
                            session.sessionCreationTime = new Date(session.sessionCreationTime);
                        });
                        return [2 /*return*/, storage];
                }
            });
        }); };
        this.getNextId = function () { return __awaiter(_this, void 0, void 0, function () {
            var sessionsCounter;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getStorageData()];
                    case 1:
                        sessionsCounter = (_a.sent()).sessionsCounter;
                        sessionsCounter++;
                        return [4 /*yield*/, this.updateStorage({ sessionsCounter: sessionsCounter })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, sessionsCounter];
                }
            });
        }); };
        this.getDefaultSession = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getStorageData()];
                    case 1: return [2 /*return*/, (_a.sent()).defaultSession];
                }
            });
        }); };
        this.setDefaultSession = function (defaultSession) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.updateStorage({
                            defaultSession: defaultSession
                        })];
                    case 1:
                        _a.sent();
                        console.log("Default session has been set: " + JSON.stringify(defaultSession) + ".");
                        return [2 /*return*/];
                }
            });
        }); };
        this.getScheduledSessions = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getStorageData()];
                    case 1: return [2 /*return*/, (_a.sent()).scheduledSessions];
                }
            });
        }); };
        this.addScheduledSession = function (session) { return __awaiter(_this, void 0, void 0, function () {
            var scheduledSessions, isAlreadySaved;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getStorageData()];
                    case 1:
                        scheduledSessions = (_a.sent()).scheduledSessions;
                        isAlreadySaved = scheduledSessions.find(function (s) { return s.id === session.id; }) ? true : false;
                        if (!!isAlreadySaved) return [3 /*break*/, 3];
                        scheduledSessions.push(session);
                        return [4 /*yield*/, this.updateStorage({ scheduledSessions: scheduledSessions })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        /**
         * Removes a scheduled session from the storage.
         * If the session doesn't exist - the function does nothing.
         */
        this.removeScheduledSession = function (sessionId) { return __awaiter(_this, void 0, void 0, function () {
            var scheduledSessions;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getStorageData()];
                    case 1:
                        scheduledSessions = (_a.sent()).scheduledSessions;
                        scheduledSessions = scheduledSessions.filter(function (s) { return s.id !== sessionId; });
                        return [4 /*yield*/, this.updateStorage({ scheduledSessions: scheduledSessions })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
    }
    Storage.prototype.updateStorage = function (updatedData) {
        return __awaiter(this, void 0, void 0, function () {
            var storage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getStorageData()];
                    case 1:
                        storage = _a.sent();
                        return [4 /*yield*/, node_persist_1.default.setItem(this.STORAGE_KEY, __assign({}, storage, updatedData))];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return Storage;
}());
exports.default = Storage;
