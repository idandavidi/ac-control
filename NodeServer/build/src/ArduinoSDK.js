"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_fetch_1 = __importDefault(require("node-fetch"));
var Arduino = /** @class */ (function () {
    function Arduino(arduinoHost, scheduledSessions, storage) {
        this.arduinoUrl = "http://" + arduinoHost;
        this.storage = storage;
        this.timers = new Map();
        this.initScheduledSessions(scheduledSessions);
    }
    Arduino.prototype.initScheduledSessions = function (scheduledSessions) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, scheduledSessions_1, session;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, scheduledSessions_1 = scheduledSessions;
                        _a.label = 1;
                    case 1:
                        if (!(_i < scheduledSessions_1.length)) return [3 /*break*/, 4];
                        session = scheduledSessions_1[_i];
                        return [4 /*yield*/, this.scheduleCommand(session)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4:
                        ;
                        return [2 /*return*/];
                }
            });
        });
    };
    Arduino.prototype.scheduleCommand = function (scheduledSession) {
        return __awaiter(this, void 0, void 0, function () {
            var now, scheduleDate, sessionCreationTime, session, scheduleInSeconds, error_1, timer;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        now = new Date();
                        scheduleDate = scheduledSession.scheduleDate, sessionCreationTime = scheduledSession.sessionCreationTime, session = __rest(scheduledSession, ["scheduleDate", "sessionCreationTime"]);
                        scheduleInSeconds = (scheduleDate.getTime() - now.getTime()) / 1000;
                        if (!(scheduleInSeconds >= -3 && scheduleInSeconds <= 0)) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.sendCommand(session)];
                    case 2:
                        _a.sent();
                        console.info("Session " + scheduledSession.id + " has been successfully sent to Arduino.");
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.error("Could not send command to arduino, session: " + JSON.stringify(scheduledSession), error_1);
                        throw error_1;
                    case 4: return [3 /*break*/, 9];
                    case 5:
                        if (!(scheduleInSeconds > 0)) return [3 /*break*/, 7];
                        timer = setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                            var error_2;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        _a.trys.push([0, 2, 3, 5]);
                                        return [4 /*yield*/, this.sendCommand(session)];
                                    case 1:
                                        _a.sent();
                                        console.info("Session " + scheduledSession.id + " has been successfully sent to Arduino.");
                                        return [3 /*break*/, 5];
                                    case 2:
                                        error_2 = _a.sent();
                                        console.error("Could not send command to arduino, session: " + JSON.stringify(scheduledSession), error_2);
                                        return [3 /*break*/, 5];
                                    case 3: return [4 /*yield*/, this.removeSession(scheduledSession.id)];
                                    case 4:
                                        _a.sent();
                                        return [7 /*endfinally*/];
                                    case 5: return [2 /*return*/];
                                }
                            });
                        }); }, scheduleInSeconds * 1000);
                        return [4 /*yield*/, this.saveSession(scheduledSession, timer)];
                    case 6:
                        _a.sent();
                        console.info("Session " + scheduledSession.id + " has been scheduled to " + scheduledSession.scheduleDate + ".");
                        return [3 /*break*/, 9];
                    case 7: return [4 /*yield*/, this.removeSession(scheduledSession.id)];
                    case 8:
                        _a.sent();
                        console.warn("Warning - scheduleInSeconds value is less than zero: " + scheduleInSeconds + ", \n" +
                            ("\tsession: " + JSON.stringify(scheduledSession) + " \n") +
                            "\tThis session has been removed.");
                        _a.label = 9;
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    Arduino.prototype.cancelScheduledSession = function (sessionId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.removeSession(sessionId)];
                    case 1:
                        _a.sent();
                        console.log("Session " + sessionId + " has been canceled.");
                        return [2 /*return*/];
                }
            });
        });
    };
    Arduino.prototype.sendCommand = function (session) {
        return __awaiter(this, void 0, void 0, function () {
            var uri, response, data, text;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        uri = this.arduinoUrl + "/command?" +
                            ("temperature=" + session.temperature + "&mode=" + session.mode + "&fanSpeed=" + session.fan + "&power=" + session.power);
                        return [4 /*yield*/, node_fetch_1.default(uri, { method: 'POST' })];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.body];
                    case 2:
                        data = _a.sent();
                        if (!!response.ok) return [3 /*break*/, 4];
                        return [4 /*yield*/, response.text()];
                    case 3:
                        text = _a.sent();
                        throw new Error("Error from arduino: " + text);
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Arduino.prototype.saveSession = function (scheduledSession, timer) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.addScheduledSession(scheduledSession)];
                    case 1:
                        _a.sent();
                        this.timers.set(scheduledSession.id, timer);
                        return [2 /*return*/];
                }
            });
        });
    };
    Arduino.prototype.removeSession = function (sessionId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // TODO add reason and save to logs
                    return [4 /*yield*/, this.storage.removeScheduledSession(sessionId)];
                    case 1:
                        // TODO add reason and save to logs
                        _a.sent();
                        clearTimeout(this.timers.get(sessionId));
                        this.timers.delete(sessionId);
                        return [2 /*return*/];
                }
            });
        });
    };
    return Arduino;
}());
exports.Arduino = Arduino;
