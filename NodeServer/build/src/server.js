"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv_1 = require("dotenv");
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var storage_1 = __importDefault(require("./storage"));
var ArduinoSDK_1 = require("./ArduinoSDK");
dotenv_1.config(); // enable reading environment variables from .env file
var app = express_1.default();
var initApp = function () { return __awaiter(_this, void 0, void 0, function () {
    var port, storage, arduino, _a, _b;
    var _this = this;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                port = parseInt(process.env.APP_PORT || "3000");
                return [4 /*yield*/, (new storage_1.default()).init()];
            case 1:
                storage = _c.sent();
                _a = ArduinoSDK_1.Arduino.bind;
                _b = [void 0, process.env.ARDUINO_HOST];
                return [4 /*yield*/, storage.getScheduledSessions()];
            case 2:
                arduino = new (_a.apply(ArduinoSDK_1.Arduino, _b.concat([_c.sent(), storage])))();
                app.use(body_parser_1.default.json());
                app.listen(port, function () {
                    console.log("Listening at http://localhost:" + port + "/");
                });
                app.use(express_1.default.static('client/dist'));
                app.post('/schedule/seconds', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var body, secondsScheduledSessions, now, scheduledSessions, defaultSession, _i, secondsScheduledSessions_1, secondsScheduledSession, _a, scheduleSeconds, scheduledSession, _b, _c, _d, scheduledSessions_1, scheduledSession, error_1, numberOfScheduledSessions;
                    return __generator(this, function (_e) {
                        switch (_e.label) {
                            case 0:
                                body = req.body;
                                if (!body.sessions || body.sessions.length == 0) {
                                    res.status(400);
                                    res.send("body.sessions array is not specified or empty.");
                                }
                                secondsScheduledSessions = body.sessions;
                                now = new Date();
                                scheduledSessions = [];
                                return [4 /*yield*/, storage.getDefaultSession()];
                            case 1:
                                defaultSession = _e.sent();
                                _i = 0, secondsScheduledSessions_1 = secondsScheduledSessions;
                                _e.label = 2;
                            case 2:
                                if (!(_i < secondsScheduledSessions_1.length)) return [3 /*break*/, 5];
                                secondsScheduledSession = secondsScheduledSessions_1[_i];
                                if (!secondsScheduledSession.hasOwnProperty('scheduleSeconds')) {
                                    res.status(400);
                                    res.send("scheduleSeconds is missing in session.");
                                    return [2 /*return*/];
                                }
                                _b = [{}, defaultSession,
                                    secondsScheduledSession];
                                _c = {};
                                return [4 /*yield*/, storage.getNextId()];
                            case 3:
                                _a = __assign.apply(void 0, _b.concat([(_c.id = _e.sent(), _c.sessionCreationTime = now, _c.scheduleDate = new Date(now.getTime() + secondsScheduledSession.scheduleSeconds * 1000), _c)])), scheduleSeconds = _a.scheduleSeconds, scheduledSession = __rest(_a, ["scheduleSeconds"]);
                                scheduledSessions.push(scheduledSession);
                                _e.label = 4;
                            case 4:
                                _i++;
                                return [3 /*break*/, 2];
                            case 5:
                                _d = 0, scheduledSessions_1 = scheduledSessions;
                                _e.label = 6;
                            case 6:
                                if (!(_d < scheduledSessions_1.length)) return [3 /*break*/, 11];
                                scheduledSession = scheduledSessions_1[_d];
                                _e.label = 7;
                            case 7:
                                _e.trys.push([7, 9, , 10]);
                                return [4 /*yield*/, arduino.scheduleCommand(scheduledSession)];
                            case 8:
                                _e.sent();
                                return [3 /*break*/, 10];
                            case 9:
                                error_1 = _e.sent();
                                res.status(500);
                                res.send("Could not schedule this session, check the server logs.");
                                return [2 /*return*/];
                            case 10:
                                _d++;
                                return [3 /*break*/, 6];
                            case 11:
                                ;
                                return [4 /*yield*/, storage.getScheduledSessions()];
                            case 12:
                                numberOfScheduledSessions = (_e.sent()).length;
                                res.send("OK - " + numberOfScheduledSessions);
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.post('/schedule/date', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var body, dateScheduledSessions, scheduledSessions, defaultSession, _i, dateScheduledSessions_1, dateScheduledSession, _a, _b, _c, _d, _e, scheduledSessions_2, scheduledSession, error_2, numberOfScheduledSessions;
                    return __generator(this, function (_f) {
                        switch (_f.label) {
                            case 0:
                                body = req.body;
                                if (!body.sessions || body.sessions.length == 0) {
                                    res.status(400);
                                    res.send("body.sessions array is not specified or empty.");
                                }
                                dateScheduledSessions = body.sessions;
                                scheduledSessions = [];
                                return [4 /*yield*/, storage.getDefaultSession()];
                            case 1:
                                defaultSession = _f.sent();
                                _i = 0, dateScheduledSessions_1 = dateScheduledSessions;
                                _f.label = 2;
                            case 2:
                                if (!(_i < dateScheduledSessions_1.length)) return [3 /*break*/, 5];
                                dateScheduledSession = dateScheduledSessions_1[_i];
                                if (!dateScheduledSession.scheduleDate) {
                                    res.status(400);
                                    res.send("scheduleDate is missing in session.");
                                    return [2 /*return*/];
                                }
                                _b = (_a = scheduledSessions).push;
                                _c = [{}, defaultSession,
                                    dateScheduledSession];
                                _d = {};
                                return [4 /*yield*/, storage.getNextId()];
                            case 3:
                                _b.apply(_a, [__assign.apply(void 0, _c.concat([(_d.id = _f.sent(), _d.sessionCreationTime = new Date(), _d.scheduleDate = new Date(dateScheduledSession.scheduleDate), _d)]))]);
                                _f.label = 4;
                            case 4:
                                _i++;
                                return [3 /*break*/, 2];
                            case 5:
                                _e = 0, scheduledSessions_2 = scheduledSessions;
                                _f.label = 6;
                            case 6:
                                if (!(_e < scheduledSessions_2.length)) return [3 /*break*/, 11];
                                scheduledSession = scheduledSessions_2[_e];
                                _f.label = 7;
                            case 7:
                                _f.trys.push([7, 9, , 10]);
                                return [4 /*yield*/, arduino.scheduleCommand(scheduledSession)];
                            case 8:
                                _f.sent();
                                return [3 /*break*/, 10];
                            case 9:
                                error_2 = _f.sent();
                                res.status(500);
                                res.send("Could not schedule this session, check the server logs.");
                                return [2 /*return*/];
                            case 10:
                                _e++;
                                return [3 /*break*/, 6];
                            case 11:
                                ;
                                return [4 /*yield*/, storage.getScheduledSessions()];
                            case 12:
                                numberOfScheduledSessions = (_f.sent()).length;
                                res.send("OK - " + numberOfScheduledSessions);
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.get('/numberOfScheduledSessions', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var numberOfScheduledSessions;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, storage.getScheduledSessions()];
                            case 1:
                                numberOfScheduledSessions = (_a.sent()).length;
                                res.send(String(numberOfScheduledSessions));
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.get('/scheduledSessions', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var scheduledSessions;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, storage.getScheduledSessions()];
                            case 1:
                                scheduledSessions = _a.sent();
                                res.status(200);
                                res.contentType('application/json');
                                res.send(scheduledSessions);
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.delete('/allScheduledSessions', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var scheduledSessions, i;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, storage.getScheduledSessions()];
                            case 1:
                                scheduledSessions = _a.sent();
                                i = 0;
                                _a.label = 2;
                            case 2:
                                if (!(i < scheduledSessions.length)) return [3 /*break*/, 5];
                                return [4 /*yield*/, arduino.cancelScheduledSession(scheduledSessions[i].id)];
                            case 3:
                                _a.sent();
                                _a.label = 4;
                            case 4:
                                ++i;
                                return [3 /*break*/, 2];
                            case 5:
                                res.send("OK");
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.get('/defaultSession', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var defaultSession;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, storage.getDefaultSession()];
                            case 1:
                                defaultSession = _a.sent();
                                res.status(200);
                                res.contentType('application/json');
                                res.send(defaultSession);
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.put('/defaultSession', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var body, currentDefaultSession, newDefaultSession;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                body = req.body;
                                return [4 /*yield*/, storage.getDefaultSession()];
                            case 1:
                                currentDefaultSession = _a.sent();
                                newDefaultSession = __assign({}, currentDefaultSession, body.defaultSession);
                                return [4 /*yield*/, storage.setDefaultSession(newDefaultSession)];
                            case 2:
                                _a.sent();
                                res.status(200);
                                res.contentType('application/json');
                                res.send(newDefaultSession);
                                return [2 /*return*/];
                        }
                    });
                }); });
                app.delete('/session/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                    var sessionId;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (isNaN(req.params.id)) {
                                    res.status(400);
                                    res.send("id " + req.params.id + " is not a number.");
                                    return [2 /*return*/];
                                }
                                sessionId = parseInt(req.params.id);
                                return [4 /*yield*/, arduino.cancelScheduledSession(sessionId)];
                            case 1:
                                _a.sent();
                                res.status(200);
                                res.send('OK');
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
        }
    });
}); };
initApp();
