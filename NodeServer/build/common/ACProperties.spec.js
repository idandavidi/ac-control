"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Power;
(function (Power) {
    Power[Power["OFF"] = 0] = "OFF";
    Power[Power["ON"] = 1] = "ON";
})(Power = exports.Power || (exports.Power = {}));
var Mode;
(function (Mode) {
    Mode[Mode["AUTO"] = 0] = "AUTO";
    Mode[Mode["COLD"] = 1] = "COLD";
    Mode[Mode["DRY"] = 2] = "DRY";
    Mode[Mode["FAN"] = 3] = "FAN";
    Mode[Mode["HEAT"] = 4] = "HEAT";
})(Mode = exports.Mode || (exports.Mode = {}));
var Fan;
(function (Fan) {
    Fan[Fan["FAN_AUTO"] = 0] = "FAN_AUTO";
    Fan[Fan["FAN_1"] = 1] = "FAN_1";
    Fan[Fan["FAN_2"] = 2] = "FAN_2";
    Fan[Fan["FAN_3"] = 3] = "FAN_3";
})(Fan = exports.Fan || (exports.Fan = {}));
