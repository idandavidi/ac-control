# Smart AC Control
This project is a web-based, mobile capable smart AC-Control built with ESP8266 and NodeJS.  
![gif](/uploads/73181c055ba133b113b1767bb270007e/ac1-small.gif)

## Components
![image](/uploads/c2dfa539142f821579a660832900ae45/image.png)

> Session is a state of remote control that is candidate to be sent to the air conditioner.


### ESP8266 Development Board
The board is used as the remote control of the AC. The board is an HTTP server that listens to HTTP Post requests and control the AC according to those requests with an IR transmitter.  
The remote is not responsible for the scheduling system, i.e it can only send instant commands to the AC system.  
- In this project the AC brand is `Tadiran`, but you can easily convert this repo to work with your AC brand (see [here](#other-ac-brands-support)).  
- In this project the development board is based on ESP8266, but you probably can use any other Arduion-like development board as a subtitue.  

> Note that the IR sensor must always have eye-contact with the air conditioner.


### NodeJS Server
The server listens to client requests on one hand, and sends requests to the development board on the other hand.  
It's responsible for the scheduling system, i.e it saves the future commands and sends them to the board when the time comes.  
The NodeJS server can run on any machine; in my case it runs on OrangePi-PC, but you can use any other machine like your PC or RaspberryPi.

## Features Review
### Remote Control Screen
In the remote control screen you can tap the icons to change the remote control state and then press the `Send` button in order to send the command to the server.  
![gif](/uploads/73181c055ba133b113b1767bb270007e/ac1-small.gif)

#### Instant Send
![instant-send](/uploads/2cc16532b73da0b99f5d7d14bc49ee06/istant-send.gif)  
> Enabling instant-send makes every remote control state change to be sent to the server instantly

### Date Schedule
![date-schedule](/uploads/a30f22b9024d47445f390e1e0595d165/date-schedule.gif)  
> Choose the DATE that the current remote control state will be sent to the AC

### Time Schedule
![time-schedule](/uploads/e4ca5689e1b8b9461f81c0ba897053bc/time-schedule.gif)  
> Choose the TIME that the current remote control state will be sent to the AC

### Fast Schedule
![fast-schedule](/uploads/7ec06411dc69307f8ddb2af951b5f9bb/fast-schedule.gif)  
> Waking up in the middle of the night and just want the AC to be turned on for the next 2 hours? Just click the button!

### Reset
![reset](/uploads/b4265c15831bc1753f3f90bdadb15643/reset.gif)
> Reset the remote control state to it's default

### Scheduled Sessions Screen
In the sessions screen you can see the future scheduled sessions that are saved in the server and will be sent to the AC.  
<img src="/uploads/968bef5a3c1129477c85fd5148bd039f/image.png"  width="320px">

#### Cancel Session
![cancel-session](/uploads/08b7c2a5ce7a359673fdfc48e0dad190/cancel-session.gif)
> You can cancel any future scheduled session anytime

## How to install
### Prepare the Board
1. Clone this repo
2. Copy `ArduinoLibraries` to your arduino libraries directory (in Windows it is usually located at `documents/Arduino/libraries`).
3. Open the sketch located in `ArduinoServer` with Arduino IDE and change the code to match your WIFI settings 
    - Search for `WIFI_SSID` and `WIFI_PASS` and change them to your local WIFI credentials
    - Search for `connectToWifi()` function and change the static ip to match your local WIFI settings (I am using `192.168.0.103` which is an IP address that is outside of the DHCP ip addresses range)
4. Connect `VS` pin of the IR led to any PWM pin, search for `IR_PIN` in the sketch and update it to your chosen pin.
4. If you have other AC brand, please refer now to [Other AC-brand support](#other-ac-brands-support).
5. Upload the program to your development board

<img src="/uploads/7dc6075bdd2699bc42f90c9aaeb6e558/nodemcu2.jpg"  width="50%" height="50%">

### Run the NodeJS Server
1. Copy `NodeServer` folder to your local machine
2. Edit `.env` file to match your configuration: select your app port and your board address mentioned above
3. Run `npm install` in the root directory (make sure you have node and npm [installed](https://www.npmjs.com/get-npm) first)
4. Change directory to `client` and run `npm install` there as well
5. Run `npm run build` under this directory (`NodeServer/client`)
6. Change directory back to root (`NodeServer`) and run `npm start`
7. Check if your app runs (defaults to run on port 3000)


## Other AC-brands support
This repo built-in AC brand support is Tadiran, which is a common brand in Israel. The IR commands should work if you have any of those remotes: YAA1FB, FAA1FB1, YB1F2, YB1FA, YAG1FB.  
If you have another brand you will need to implement some code under the library located in `ArduinoLibraries/Tadiran`. You may want to change this library name to fir your AC brand name.  
You will have to change the constructor, `setTemeprature`, `setMode`, `setState` and `setFan` methods to fits your remote-control IR codes. You should search in the internet first - maybe somebody already implemented this logic :smile:  
This may be the tricky part, so take your time to learn this subject. Here are some references:
- Controling AC with arduino and IR led (HEBREW): https://www.youtube.com/watch?v=67IEmopqE9A&t=2s

If you implemented a library for your brand you are welcome to create a MR to add your code to the repo `ArduinoLibraries`!

## Credits
1. [Zapatta/MySensors](https://github.com/Zappatta/MySensors/tree/master/AcControl/Library) repo in github. I have used it's implementation of the IR library for Tadiran control.
2. [https://www.flaticon.com/](https://www.flaticon.com/) for the icons used in the client-side user interface.
