#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Adafruit_NeoPixel.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <Tadiran.h>

#define IR_PIN D8
#define LED_BUILTIN D4
#define NEOPIXEL_PIN D1
#define NEOPIXEL_LEDNUM 4
#define PORT 80
#define WIFI_SSID "netvision"
#define WIFI_PASS "10203040"

ESP8266WebServer server(PORT);

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NEOPIXEL_LEDNUM, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);

// the IR emitter object
IRsend irsend(IR_PIN);
int temperature = 24;
int mode = MODE_cold; // 0-Auto | 1-Cold | 2-Dry | 3-Fan | 4-Heat
int fanspeed = FAN_1; //0-Auto | 1-Low | 2-Medium | 3-High

Tadiran tadiran(mode, fanspeed, temperature, STATE_on);

void setup() {
  Serial.begin(115200);
  Serial.println("hello!");
  pinMode(IR_PIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // Turn led off
  pinMode(LED_BUILTIN, OUTPUT);
  strip.begin();
  strip.setBrightness(20);
  Serial.println("Commands: +/- Temperature | m - Mode | f - fanspeed | p - Power");

  connectToWifi();
  initRoutes();
}

void connectToWifi() {
  IPAddress ip(192,168,0,103);
  IPAddress gateway(192,168,0,1);
  IPAddress subnet(255,255,255,0);
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  Serial.printf("Connecting to %s", WIFI_SSID);
  Serial.print("..");

  // Wait for the Wi-Fi to connect
  while (WiFi.status() != WL_CONNECTED) {
    loading(1000);
    Serial.print('.');
  }

  success(1000);
  Serial.print('\n');
  Serial.println("Connection established!");
  Serial.print("IP address: ");
  Serial.print(WiFi.localIP());
  Serial.println('\n');
}

void loading(int duration) {
  double delayTime = (double) duration / (strip.numPixels() + 1) / 2;
  for(int i=0; i<strip.numPixels(); ++i) {
    strip.setPixelColor(i, strip.Color(0,0,255));
    strip.show();
    delay(delayTime);
  }
  strip.clear();
  strip.show();
  delay(delayTime);
}

void success(int duration) {
  success(duration, strip.Color(0,255,0));
}

void success(int duration, int color) {
  int flashTimes = 2;
  double delayTime = (double) duration / flashTimes / 2;
  
  for (int j=0; j<flashTimes; ++j) {
    for(int i=0; i<strip.numPixels(); ++i) {
      strip.setPixelColor(i, color);
    }
    
    strip.show();
    delay(delayTime);
    strip.clear();
    strip.show();
    delay(delayTime);
  }
}

void initRoutes() {
  server.on("/", HTTP_GET, handleRoot);
  server.on("/command", HTTP_POST, handleCommand);
  server.onNotFound(handleNotFound);

  server.begin();
}

void handleRoot() {
  server.send(200, "text/plain", "Hello world!");
}

void handleCommand() {
  if (!server.hasArg("temperature") || !server.hasArg("mode") ||
      !server.hasArg("fanSpeed") || !server.hasArg("power")) {
    server.send(400, "text/plain", "Some arguments are missing.");
    Serial.printf("/tadiran 400 \n\n");
    return;
  }

  int temperature = server.arg("temperature").toInt();
  int mode = server.arg("mode").toInt();
  int fanSpeed = server.arg("fanSpeed").toInt();
  int power = server.arg("power").toInt(); // 1="on", anything else="off"

  tadiran.setTemeprature(temperature);
  tadiran.setMode(mode);
  tadiran.setFan(fanSpeed);
  tadiran.setState(power == 1 ? 1 : 0);

  irsend.sendRaw(tadiran.codes, TADIRAN_BUFFER_SIZE, 38);
  
  server.send(200, "text/plain", "OK");

  Serial.printf("/tadiran 200 - temperature: %i, mode: %i, fanSpeed: %i, power: %i\n",
    temperature, mode, fanSpeed, power);
  tadiran.print();
  Serial.println();
  blink();              
}

void blink() {
  success(400, strip.Color(0,0,255));
}

void handleNotFound() {
  server.send(404, "text/plain", "404: Not found");
}

void loop() {
  server.handleClient();
}
